package com.jago.pokemon

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@ConfigurationPropertiesScan
@SpringBootApplication
class MsPokemonApplication

fun main(args: Array<String>) {
    runApplication<MsPokemonApplication>(*args)
}
