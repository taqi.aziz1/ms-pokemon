package com.jago.pokemon.services.pokemon

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

@ConfigurationProperties("app.poke-api")
data class PokemonProperties(
    val url: String,
)

@Service
class PokemonConfiguration {
    @Bean
    fun pokeWebClient(): WebClient {
        return WebClient.create()
    }
}