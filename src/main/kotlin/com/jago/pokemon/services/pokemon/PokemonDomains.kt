package com.jago.pokemon.services.pokemon

import java.net.URL

data class ListPokemonDto(
    val count: Int,
    val next: URL?,
    val previous: URL?,
    val results: List<Info>,
)

data class Pokemon(
    val id: Int,
    val name: String,
    val order: Int,
    val isDefault: Boolean,
    val species: Info,
    val types: List<Type>,

    val abilities: List<Ability>,
    val baseExperience: Int,
    val forms: List<Info>,
    val moves: List<Move>,
    val gameIndices: List<GameIndex>,
    val height: Int,
    val weight: Int,

    val sprites: Sprites,
    val stats: List<Stat>,

    val heldItems: List<Any>,
    val pastTypes: List<Any>,
    val locationAreaEncounters: URL,
) {

    data class Ability(
        val ability: Info,
        val isHidden: Boolean,
        val slot: Int,
    )

    data class GameIndex(
        val gameIndex: Int,
        val version: Info,
    )

    data class Move(
        val move: Info,
        val versionGroupDetails: List<VersionGroupDetail>,
    ) {

        data class VersionGroupDetail(
            val levelLearnedAt: Int,
            val moveLearnMethod: Info,
            val versionGroup: Info,
        )
    }

    data class Sprites(
        val frontDefault: URL?,
        val frontFemale: URL?,
        val frontShiny: URL?,
        val frontShinyFemale: URL?,

        val backDefault: URL?,
        val backFemale: URL?,
        val backShiny: URL?,
        val backShinyFemale: URL?,

        val other: Map<String, Sprites>?,
        val versions: Map<String, Any>?,
    )

    data class Stat(
        val baseStat: Int,
        val effort: Int,
        val stat: Info,
    )

    data class Type(
        val slot: Int,
        val type: Info,
    )
}

data class Info(
    val name: String,
    val url: URL,
)