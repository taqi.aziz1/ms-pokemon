package com.jago.pokemon.services.pokemon

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlux
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class PokemonService(
    private val pokeWebClient: WebClient
) {
    fun listPokemon(): Flux<Info> {
        return pokeWebClient
            .get()
            .uri("/pokemon")
            .retrieve()
            .bodyToMono<ListPokemonDto>()
            .flatMapIterable { it.results }
    }
}