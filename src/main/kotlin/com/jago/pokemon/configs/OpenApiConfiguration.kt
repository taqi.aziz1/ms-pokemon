package com.jago.pokemon.configs

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@ConfigurationProperties("app.open-api")
data class OpenApiProperties(
    val title: String,
    val version: String,
    val description: String,
)

@Configuration
class OpenApiConfiguration {

    @Bean
    fun openApi(properties: OpenApiProperties): OpenAPI {
        return OpenAPI()
            .info(
                Info()
                    .title(properties.title)
                    .version(properties.version)
                    .description(properties.description)
            )
    }
}