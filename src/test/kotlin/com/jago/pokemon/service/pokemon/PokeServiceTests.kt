package com.jago.pokemon.service.pokemon

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.jago.pokemon.services.pokemon.*
import io.mockk.junit5.MockKExtension
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.jeasy.random.EasyRandom
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier

val random = EasyRandom()

val objectMapper: ObjectMapper = jacksonObjectMapper().registerModules(JavaTimeModule(), Jdk8Module())!!

fun MockWebServer.enqueueJson(status: HttpStatus, payload: Any? = null) {
    if (payload != null) {
        val body: String = when (payload) {
            is String -> payload
            is ClassPathResource -> payload.file.readText()
            else -> objectMapper.writeValueAsString(payload)
        }

        this.enqueue(
            MockResponse()
                .setResponseCode(status.value())
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .addHeader(HttpHeaders.CONNECTION, "close")
                .setBody(body),
        )
    } else {
        this.enqueue(
            MockResponse()
                .setResponseCode(status.value())
                .addHeader(HttpHeaders.CONNECTION, "close"),
        )
    }
}

@ExtendWith(MockKExtension::class)
internal class PokeServiceTests {

    private val pokeWebServer = MockWebServer()
    private val pokeProperties = PokemonProperties(pokeWebServer.url("").toString())
    private val pokeWebClient = WebClient.create(pokeProperties.url)
    private val pokeService = PokemonService(pokeWebClient)

    @Nested
    inner class ListPokemon {
        @Test
        fun `Given 200 - When called - Should return result as Flux`() {
            // Given
            val pokemon1 = random.nextObject(Info::class.java)
            val pokemon2 = random.nextObject(Info::class.java)
            val pokemon3 = random.nextObject(Info::class.java)
            val pokemon4 = random.nextObject(Info::class.java)
            val pokemon5 = random.nextObject(Info::class.java)

            val response = ListPokemonDto(5, null, null, listOf(pokemon1, pokemon2, pokemon3, pokemon4, pokemon5))

            // When
            pokeWebServer.enqueueJson(HttpStatus.OK, response)

            // Then
            StepVerifier.create(pokeService.listPokemon())
                .expectSubscription()
                .expectNext(pokemon1)
                .expectNext(pokemon2)
                .expectNext(pokemon3)
                .expectNext(pokemon4)
                .expectNext(pokemon5)
                .expectComplete()
                .verify()
        }
    }
}
